# [neonode-mobile.appspot.com](http://neonode-mobile.appspot.com/)

[neonode-mobile](http://neonode-mobile.appspot.com/) är en webb-app för mobila enheter. Appen, i sitt nuvarande utförande, bygger på [Sencha Touch v. 2.2.0](http://www.sencha.com/blog/all-new-sencha-cmd) och är primärt ämnad att fungera som _demo-case_ eller _proof of concept_.

## Key features

* Litet _footprint_, hela appen – inkl. omkring 40 sidvyer och ett dussintal bilder – tar inte mer än 1 MB i anspråk.
* Allt innehåll förutom  meny-alternativen läses in och renderas dynamiskt
* Efter att användaren besökt webb-appen en första gång förblir allt innehåll tillgängligt offline via webbläsarens cache.
* Appen har en [responsiv design](http://en.wikipedia.org/wiki/Responsive_web_design) och ska fungera tillfredställande, i _portrait_- såväl som _lanscape_-läge, på alla modernare mobila enheter – från budget-mobiler med Android eller Windows Phone som OS till Apples senaste tablet.
* Färdig att [packeteras](http://docs.sencha.com/touch/2.2.0/#!/guide/native_packaging) för App Store och Google Play via Sencha Cmd.

## Historik

[neonode-mobile](http://neonode-mobile.appspot.com/) såg dagens ljus först den 15 januari 2012. Den första versionen av appen tog 40-50 utvecklingstimmar i anspråk och byggdes med v. 2.0.0rc3 av Sencha Touch. Syftet med projektet var att stimulera intresset för mobila webb-appar som alternativ till s.k. "native-appar".

Appen har, sedan lanseringen, legat publicerad på [Google App Engine](https://developers.google.com/appengine/) som demo. Den 17 april 2013 lanserade Sencha v. 2.2.0 av sitt webbapplikationsramverk Sencha Touch. Med version 2.2.0 introducerades bl.a.

* stabilt stöd för IE10 och
* en ny lösning för att bygga egna teman, från scratch, via [Sencha Cmd 3](http://docs.sencha.com/touch/2.2.0/#!/guide/command).

Den 22 april 2013 togs appen tillfälligt ner från App Engine för att uppdateras till senaste versionen av Sencha Touch. En dag senare var appen färdig att återpubliceras i sitt nuvarande utförande.

## Tillkortakommanden och vidareutvecklingsuppslag

Appen innehåller, i sitt nuvarande utförande, en replika av det innehåll som fanns publicerat på Neonodes WordPress-baserade desktop-sajt – [www.neonode.com](http://www.neonode.com) – vid årsskiftet 2011–12.

För ta fram en produktionsfärdig implementation skulle appen behöva kompletteras med några ytterligare komponenter:

* _Back-end_-stöd för att löpande fylla på med dynamiskt innehåll, antingen via integrering med ett CMS eller inläsning av ett JSON-/XML-flöde.
* _Front-end_-funktionalitet för att
    * upprätthålla och synkronisera en LocalStorage-databas hos klienten, samt
    * detektera när klienten har en fungerande internetanslutning för att – asynkront och i bakgrunden – kunna lägga till, uppdatera och ta bort innehåll på ett sätt som är minimalt "intrusive".
