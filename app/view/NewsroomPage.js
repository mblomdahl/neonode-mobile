Ext.define( 'Neonode.view.NewsroomPage', {
    extend: 'Ext.Panel',
    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.model.NewsroomModel'
    ],
    alias: 'widget.newsroompage',
    id: 'newsroomPage',
    initialize: function () {
        this.populate = function ( target ) {
            if ( !target.node ) {
                return false;
            }
            console.log( "this.id" + this.id );
            var storeIndex = window.parseInt( target.node ),
                targetItem = this.getItems().map[this.id + '-view'].items.items[1];

            targetItem.setHtml( GeneratorModel.newsDetails( storeIndex, target.id, target.backLocation ) );

            return true;
        }
    },
    config: {
        fullscreen: true,
        layout: 'card',
        style: 'background: black;',
        items: [
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'newsroomPage-intro',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'NEWSROOM' )
                    },
                    {
                        html: NewsroomModel.intro( 'newsroomPage-intro' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'newsroomPage-press',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'NEWSROOM' )
                    },
                    {
                        html: NewsroomModel.press( 'newsroomPage-press' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'newsroomPage-news',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'NEWSROOM' )
                    },
                    {
                        html: NewsroomModel.news( 'newsroomPage-news' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'newsroomPage-view',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'NEWSROOM' )
                    },
                    {
                        html: ''
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            }
            /*{
             xtype: 'panel',
             scrollable: 'vertical',
             id: 'newsroomPage-video',
             layout: 'vbox',
             items: [
             {
             html: GeneratorModel.menu('NEWSROOM')
             },
             {
             html: NewsroomModel.video('newsroomPage-video')
             },
             {
             html: GeneratorModel.footer()
             }
             ]
             }*/
        ]
    }
} );
