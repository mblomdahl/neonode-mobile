Ext.define( 'Neonode.view.InvestorRelationsPage', {
    extend: 'Ext.Panel',
    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.model.InvestorRelationsModel'
    ],
    alias: 'widget.investorrelationspage',
    id: 'investorRelationsPage',
    config: {
        scrollable: 'vertical',
        fullscreen: true,
        style: 'background: black;',
        layout: 'vbox',
        items: [
            {
                html: GeneratorModel.menu( 'INVESTOR RELATIONS' )
            },
            {
                html: InvestorRelationsModel.intro( 'investorRelationsPage' )
            },
            {
                html: GeneratorModel.menu()
            }
        ]
    }
} );
