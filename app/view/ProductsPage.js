Ext.define('Neonode.view.ProductsPage', {
  extend: 'Ext.Panel',
  requires: [
    'Neonode.model.GeneratorModel',
    'Neonode.model.ProductsModel'
  ],
  alias: 'widget.productspage',
  id: 'productsPage',
  config: {
    fullscreen: true,
    layout: 'card',
    style: 'background: black;',
    items: [
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-intro',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.intro('productsPage-intro')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-nn1001',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.nn1001('productsPage-nn1001')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-mobile',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.mobile('productsPage-mobile')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-tablet',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.tablet('productsPage-tablet')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-automotive',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.automotive('productsPage-automotive')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-office',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.office('productsPage-office')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-home',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.home('productsPage-home')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
      {
        xtype: 'panel',
        scrollable: 'vertical',
        id: 'productsPage-toys',
        layout: 'vbox',
        items: [
          {
            html: GeneratorModel.menu('PRODUCTS')
          },
          {
            html: ProductsModel.toys('productsPage-toys')
          },
          {
            html: GeneratorModel.footer()
          }
        ]
      },
    ]
  }
});
