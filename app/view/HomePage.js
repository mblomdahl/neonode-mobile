Ext.define( 'Neonode.view.HomePage', {
    extend: 'Ext.Panel',
    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.model.HomeModel'
    ],
    alias: 'widget.homepage',
    id: 'homePage',
    initialize: function () {
        this.populate = function ( target ) {
            if ( !target.node ) {
                return false;
            }

            var storeIndex = window.parseInt( target.node ),
                targetItem = this.getItems().map[this.id + '-view'].items.items[1];

            targetItem.setHtml( GeneratorModel.newsDetails( storeIndex, target.id, target.backLocation ) );

            return true;
        }
    },
    config: {
        fullscreen: true,
        style: 'background: black;',
        layout: 'card',
        items: [
            {
                id: 'homePage-intro',
                scrollable: 'vertical',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'HOME' )
                    },
                    {
                        html: HomeModel.intro( 'homePage-intro' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                id: 'homePage-view',
                scrollable: 'vertical',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'HOME' )
                    },
                    {
                        html: ''
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            }
        ]
    }
} );

