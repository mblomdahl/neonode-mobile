Ext.define( 'Neonode.view.ContactPage', {
    extend: 'Ext.Panel',
    requires: [
        'Neonode.model.GeneratorModel',
        //'Neonode.model.ContactModel'
    ],
    alias: 'widget.contactpage',
    id: 'contactPage',
    config: {
        layout: 'card',
        fullscreen: true,
        style: 'background: black;',
        items: [
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'contactPage-headoffice',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'CONTACT' )
                    },
                    {
                        html: ContactModel.headoffice( 'contactPage-headoffice' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'contactPage-europe',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'CONTACT' )
                    },
                    {
                        html: ContactModel.europe( 'contactPage-europe' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'contactPage-usa',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'CONTACT' )
                    },
                    {
                        html: ContactModel.usa( 'contactPage-usa' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'contactPage-asia',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'CONTACT' )
                    },
                    {
                        html: ContactModel.asia( 'contactPage-asia' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            }
        ]
    }
} );

