Ext.define( 'Neonode.view.AboutPage', {
    extend: 'Ext.Panel',
    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.model.AboutModel'
    ],
    alias: 'widget.aboutpage',
    id: 'aboutPage',
    config: {
        layout: 'card',
        fullscreen: true,
        style: 'background: black;',
        items: [
            {
                xtype: 'panel',
                id: 'aboutPage-corporate',
                layout: 'card',
                items: [
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-corporate-revised',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.corporate.revised( 'aboutPage-corporate-revised' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-corporate-original',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.corporate.original( 'aboutPage-corporate-original' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'aboutPage-background',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'ABOUT' )
                    },
                    {
                        html: AboutModel.background( 'aboutPage-background' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'aboutPage-management',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'ABOUT' )
                    },
                    {
                        html: AboutModel.management( 'aboutPage-management' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                scrollable: 'vertical',
                id: 'aboutPage-board',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'ABOUT' )
                    },
                    {
                        html: AboutModel.board( 'aboutPage-board' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                id: 'aboutPage-career',
                layout: 'card',
                items: [
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-career-intro',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.career.intro( 'aboutPage-career-intro' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-career-projectmanager',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.career.projectmanager( 'aboutPage-career-projectmanager' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-career-mecheng',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.career.mecheng( 'aboutPage-career-mecheng' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-career-optoeng',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.career.optoeng( 'aboutPage-career-optoeng' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-career-electroeng',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.career.electroeng( 'aboutPage-career-electroeng' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        scrollable: 'vertical',
                        id: 'aboutPage-career-syseng',
                        layout: 'vbox',
                        items: [
                            {
                                html: GeneratorModel.menu( 'ABOUT' )
                            },
                            {
                                html: AboutModel.career.syseng( 'aboutPage-career-syseng' )
                            },
                            {
                                html: GeneratorModel.footer()
                            }
                        ]
                    }
                ]
            }
        ]
    }
} );

