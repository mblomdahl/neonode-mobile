Ext.define( 'Neonode.view.FutureConceptPage', {
    extend: 'Ext.Panel',
    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.model.FutureConceptModel'
    ],
    alias: 'widget.futureconceptpage',
    id: 'futureConceptPage',
    config: {
        layout: 'card',
        fullscreen: true,
        style: 'background: black;',
        items: [
            {
                xtype: 'panel',
                id: 'futureConceptPage-intro',
                scrollable: 'vertical',
                layout: 'vbox',
                items: [
                    {
                        html: GeneratorModel.menu( 'FUTURE CONCEPT' )
                    },
                    {
                        html: FutureConceptModel.intro( 'futureConceptPage-intro' )
                    },
                    {
                        html: GeneratorModel.footer()
                    }
                ]
            },
            {
                xtype: 'panel',
                id: 'futureConceptPage-gallery',
                layout: 'vbox',
                items: [
                    {
                        style: 'overflow: visible;',
                        height: 85,
                        html: [
                            GeneratorModel.menu( 'FUTURE CONCEPT' ), '<div class="future-concept-bg"></div>'
                        ].join( '' )
                    },
                    {
                        xtype: 'carousel',
                        ui: 'light',
                        flex: 1,
                        items: FutureConceptModel.gallery( 'futureConceptPage-gallery' )
                    }
                ]
            }
        ]
    }
} );
