Ext.define( 'Neonode.model.NewsroomModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'NewsroomModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel'
    ],

    intro: function ( id ) {
        return this.latest( id );
    },
    latest: function ( id ) {
        var store = Ext.getStore( 'NewsroomStore' ),
            content = [
                '<div class="content-wrapper2 t-layout"><h1>Newsroom</h1>',
                '<p>Neonode is at the forefront of innovation and growth. Our newsroom provides you with the latest company news and press releases as well as past announcements, investor relations and articles derived from international media attention.</p>',
                '<h1>Latest news & press releases</h1>',
                (store ? '</div>' + GeneratorModel.newsPuffs( id.split( '-' )[0] + '-view', store.getRange( 0, store.getTotalCount() ) ) : '<p class="loading">[ Loading news and press releases . . . ]</p></div>')
            ];

        return this.wrapper( id, content.join( '' ) );
    },
    press: function ( id ) {
        var store = Ext.getStore( 'NewsroomStore' ),
            content = [
                '<div class="content-wrapper2 t-layout"><h1>Press releases</h1>',
                '<p>Neonode is at the forefront of innovation and growth. Our newsroom provides you with the latest company news and press releases as well as past announcements, investor relations and articles derived from international media attention.</p>',
                (store ? '</div>' + GeneratorModel.newsPuffs( id.split( '-' )[0] + '-view', store.queryBy(function ( item ) {
                    if ( item.data.category && item.data.category.toLowerCase() === "press" ) {
                        return true;
                    }
                } ).items ) : '<p class="loading">[ Loading press releases . . . ]</p></div>')
            ];

        return this.wrapper( id, content.join( '' ) );
    },
    news: function ( id ) {
        var store = Ext.getStore( 'NewsroomStore' ),
            content = [
                '<div class="content-wrapper2 t-layout"><h1>News</h1>',
                '<p>Neonode is at the forefront of innovation and growth. Our newsroom provides you with the latest company news and press releases as well as past announcements, investor relations and articles derived from international media attention.</p>',
                (store ? '</div>' + GeneratorModel.newsPuffs( id.split( '-' )[0] + '-view', store.queryBy(function ( item ) {
                    if ( item.data.category && item.data.category.toLowerCase() === "news" ) {
                        return true;
                    }
                } ).items ) : '<p class="loading">[ Loading news . . . ]</p></div>')
            ];

        return this.wrapper( id, content.join( '' ) );
    },
    wrapper: function ( activePage, innerContent ) {
        var response = [
            '<div class="content-wrapper1">',
            GeneratorModel.submenu( [
                {
                    label: 'Content filter',
                    activeItem: activePage,
                    items: [
                        {
                            label: 'Latest',
                            page: 'newsroomPage-intro'
                        },
                        {
                            label: 'Press releases',
                            page: 'newsroomPage-press'
                        },
                        {
                            label: 'News',
                            page: 'newsroomPage-news'
                        }/*,
                         {
                         label: 'Video',
                         page: 'newsroomPage-video'
                         }*/
                    ]
                }
            ] ),
            innerContent,
            '<div class="content-wrapper3"></div></div>'
        ];

        return response.join( '' );
    }
} );
