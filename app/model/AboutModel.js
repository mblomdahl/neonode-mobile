Ext.define( 'Neonode.model.AboutModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'AboutModel',
    singleton: true,

    config: {
        fields: [
            { name: 'ContactModel', type: 'auto' }
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel'
    ],

    corporate: {
        original: function ( id ) {
            var content = [
                '<h1>Corporate information</h1>',
                '<h2>Pioneering touch technologies</h2>',
                '<p>Neonode is a world leading developer of optical touch technology for small & medium size devices. Today you find our top performing solution in world leadning e-reader, mobile phone, automotive, printer and tablet consumer brand solutions.</p>',
                '<h2>Our technology and engineering design services shorten your time-to-market</h2>',
                '<p>We offer flexible and affordable technology licenses based on our patented and award-winning technology zForce ® – the only viable touch solution that operates on the new revolutionary reflective display panels. We also include engineering design services that enable equipment and device manufacturers to produce the market’s most high performing touch screens at an incomparably low cost.</p>',
                '<h2>Value base</h2>',
                '<p>Two key words underline everything we do at Neonode: <b>“Creativity and Efficiency”</b>.</p>',
                '<p>Creativity means we solve problems in new and intelligent ways. We’re flexible to customer’s special needs and we want to improve fully functional existing platforms as well as adding something extra to everything we deliver.</p>',
                '<p>Efficiency means we want things done and completed on time and with the highest quality. Delivering more to our customers for less is a mindset. And we never stop thinking about how we can save costs for our customers as well as for ourselves.<br>',
                '<br>Neonode Inc. is listed on the OTC Bulletin Board under the symbol NEON.OB. Neonode and zForce are registered trademarks of Neonode Inc.',
                '<br><br><a href="#contactPage" class="menu-item">Contact us for more information</a></p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        revised: function ( id ) {
            var safeFont = Ext.os.is.iOS || Ext.os.is.Desktop ? '' : 'font-family: sans-serif;',
                content = [
                    '<p style="font-size: 14px;' + safeFont + '"><i>Revisionen syftar till att skapa en mer koncis och kärnfull presentation av Neonode. Originalet hade framförallt brister i from av alltför långa rubriker och för långa/klumpiga meningar – rubrikerna bör vara lättlästa och snabbt fånga uppmärksamheten. För att göra texten mer lättillgänglig har skrivit om texten med kortare meningar och ett tonläge som framhäver ambition och målsättningar snarare än en ren beskrivning av vad företaget sysslar med.</i></p>',
                    '<p style="font-size: 14px;' + safeFont + '"><i>Utöver detta kan jag bara notera att originalet både innehåller stavfel och en hel del svengelska: "Special needs" innebär på engelska, i allmänhet, funktionshinder. Man vill nog undvika att använda det uttrycket om sina kunder.</i></p>',
                    '<p style="text-align: right; font-size: 14px; padding-right: 4%; margin-bottom: 21px;"><a href="mailto:c.palmstierna@gmail.com">– c.palmstierna</a></p>',
                    '<h1>Corporate information</h1>',
                    '<h2>Touch technology is our passion</h2>',
                    '<p>At Neonode we strive to develop the best optical touch technology for small and medium size devices. We are world-leading in brand solutions for e-readers, mobile phones, automotives, printers and tablets.</p>',
                    '<h2>Let us shorten your time-to-market</h2>',
                    '<p>Our flexible, affordable technology licenses and engineering design services help equipment and device manufacturers produce the most high-performing touch screens on the market at unparallelled prices. For the new, revolutionary reflective display panels <a href="#productsPage-intro" class="menu-item">zForce</a> is the only viable solution.</p>',
                    '<h2>Vision</h2>',
                    '<p>At Neonode we are guided by two core concepts: Creativity and Efficiency. For us, creativity means novel and intelligent problem solving. By being flexible, we accomodate the individual needs of each customer. We aim to improve existing, fully functional platforms as well as going the extra mile in all our projects. Our efficiency consists of completing our tasks on time and with the highest possible quality. Our vision is to deliver more for less and to never stop looking for new ways to reduce costs for our customers as well as for ourselves.<br>',
                    '<br>Neonode Inc. is listed on the OTC Bulletin Board under NEON.OB. Neonode and zForce are registered trademarks of Neonode Inc.<br>',
                    '<br><a href="#contactPage" class="menu-item">Contact us for more information</a></p>'
                ];

            return this.wrapper( id, content.join( '' ) );
        },
        wrapper: function ( activePage, innerContent ) {
            var auxActivePage = activePage.split( '-' ),
                response = [
                    '<div class="content-wrapper1">',
                    GeneratorModel.submenu( [
                        {
                            label: 'Sub-pages',
                            activeItem: auxActivePage[0] + '-' + auxActivePage[1],
                            items: [
                                {
                                    label: 'Corporate information',
                                    page: 'aboutPage-corporate'
                                },
                                {
                                    label: 'Background',
                                    page: 'aboutPage-background',
                                    bpMobile: true
                                },
                                {
                                    label: 'Career',
                                    page: 'aboutPage-career',
                                    bpTablet: true
                                },
                                {
                                    label: 'Management',
                                    page: 'aboutPage-management'
                                },
                                {
                                    label: 'Board of Directors',
                                    page: 'aboutPage-board'
                                }
                            ]
                        },
                        {
                            label: 'Revisions',
                            activeItem: activePage,
                            items: [
                                {
                                    label: 'Copy\'s revised presentation',
                                    page: 'aboutPage-corporate-revised'
                                },
                                {
                                    label: 'Original presentation',
                                    page: 'aboutPage-corporate-original',
                                }
                            ]
                        }
                    ] ),
                    '<div class="content-wrapper2">',
                    innerContent,
                    '</div><div class="content-wrapper3"></div></div>'
                ];
            return response.join( '' );
        }
    },
    background: function ( id ) {
        var content = [
            '<h1>Background</h1>',
            '<p>Neonode’s business model is based on technology licensing to equipment- and device manufacturers on a global market. Extensive R&D has led to several new innovations the past two years: ClearTouch, Selected Area Touch (SAT) and alwaysONTM.</p>',
            '<h2>Pioneering</h2>',
            '<p>The company vision and mission was (and still is) based on the idea that the optical touch screen is the perfect technology for a completely new and user-friendly type of mobile phone – without buttons and a large, clear screen. Removing the traditional keypad and replacing it with an on-screen keyboard based on infrared light beams forming an invisible grid over the surface, made it possible to sweep, gesture and tap – without stylus or any pressure.</p>',
            '<p>This took place almost three years before the first Apple mobile phone reached the market.</p>',
            '<p>The world’s first finger-based mobile touch phone, N1, based on the proprietary patented optical touch technology zForce ® was released to the market in 2004 and attracted a large fan base. The next version, an ultra-slim device, N2, was released in 2007 – the year Neonode listed on Nasdaq.</p>',
            '<p>Neonode changed the business model in 2008 to a B2B technology licensing of zForce ® to equipment and device manufacturers on a global market. Extensive R&D has led to several new innovations these past two years: ClearTouch, Selected Area Touch (SAT) and allwaysON.</p>',
            '<p>Since 2009, Neonode has signed major contracts with a number of global equipment and device manufaturers.</p>',
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    management: function ( id ) {
        var content = [
            '<h1>Management</h1>',
            '<p>Neonode is an entrepreneurial company with an experienced management team to match.</p>',
            '<h2>Thomas Eriksson, CEO and co-founder</h2>',
            '<p><a href="mailto:thomas.eriksson@neonode.com" class="inline-anchor">thomas.eriksson@neonode.com</a></p>',
            '<h2>David Brunton, CFO</h2>',
            '<p><a href="mailto:david.brunton@neonode.com" class="inline-anchor">david.brunton@neonode.com</a></p>',
            '<h2>Doug Young, VP North America</h2>',
            '<p><a href="mailto:doug.young@neonode.com" class="inline-anchor">doug.young@neonode.com</a></p>',
            '<h2>Yossi Shain, Head of IP</h2>',
            '<p><a href="mailto:yossi.shain@neonode.com" class="inline-anchor">yossi.shain@neonode.com</a></p>',
            '<h2>Annica Englund, Marketing Director</h2>',
            '<p><a href="mailto:annica.englund@neonode.com" class="inline-anchor">annica.englund@neonode.com</a></p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    board: function ( id ) {
        var content = [
            '<h1>Board of Directors</h1>',
            '<h2>Per Bystedt, Executive Chairman</h2>',
            '<p><a href="mailto:per.bystedt@neonode.com" class="inline-anchor">per.bystedt@neonode.com</a><br>',
            'Investor and shareholder in Neonode since 2004. Per Bystedt has been Executive Chairman of Neonode Inc. since May 2008. Per has served as Chief Executive Officer of Neonode Inc. from 2008 to 2011 and served as its President since 2008. He has served as Chief Executive Officer of various television production and network companies including Trash Television, ZTV AB, TV3 Broadcasting Group Ltd and MTG AB. Per also served as Chief Executive Officer and President of Spray AB and Spray Ventures AB. He serves as Chairman of Efti AB and as Chairman of SBE Inc., eBuilder AB and Eniro AB since 2000. From 1998 to 2000, he served as Chairman of Razorfish Inc. He has been a Director of Axel Johnson AB since 2000 and of Servera AB since 2005. From 1997 to 2005, he served as a Director of Ahlens AB. He has been the Chairman of AIK Fotboll AB since 2004. Per holds an MBA and M.Sc. Econ. degree from Stockholm School of Economics.</p>',
            '<h2>Per Bystedt, Executive Chairman</h2>',
            '<p><a href="mailto:per.bystedt@neonode.com" class="inline-anchor">per.bystedt@neonode.com</a><br>',
            'Investor and shareholder in Neonode since 2004. Per Bystedt has been Executive Chairman of Neonode Inc. since May 2008. Per has served as Chief Executive Officer of Neonode Inc. from 2008 to 2011 and served as its President since 2008. He has served as Chief Executive Officer of various television production and network companies including Trash Television, ZTV AB, TV3 Broadcasting Group Ltd and MTG AB. Per also served as Chief Executive Officer and President of Spray AB and Spray Ventures AB. He serves as Chairman of Efti AB and as Chairman of SBE Inc., eBuilder AB and Eniro AB since 2000. From 1998 to 2000, he served as Chairman of Razorfish Inc. He has been a Director of Axel Johnson AB since 2000 and of Servera AB since 2005. From 1997 to 2005, he served as a Director of Ahlens AB. He has been the Chairman of AIK Fotboll AB since 2004. Per holds an MBA and M.Sc. Econ. degree from Stockholm School of Economics.</p>',
            '<h2>John Reardon, Director</h2>',
            '<p>On the Neonode board since 2007. John Reardon has served as a director of SBE Inc. since February 2004 and Neonode since February 2007. John is the Chairman of the Audit Committee and member of the Compensation and Nominating and Governance Committees of the Company. John has served as President and member of the Board of Directors of The RTC Group, a technical publishing company, since 1990. In 1994, John founded a Dutch corporation, AEE, to expand the activities of The RTC Group into Europe. John also serves on the Board of Directors of One Stop Systems Inc., a computing systems and manufacturing company. John holds a BA degree from National University.</p>',
            '<h2>Thomas Eriksson, Director and co-founder</h2>',
            '<p><a href="mailto:thomas.eriksson@neonode.com" class="inline-anchor">thomas.eriksson@neonode.com</a><br>',
            'Thomas Eriksson co-founded Neonode Inc. in 2001 and served as its Vice President and Chief Technology Officer until 2009 when he became the company’s Chief Executive Officer. Prior to founding Neonode AB, he founded several companies with products ranging from car electronics test systems and tools to GSM/GPRS/GPS-based fleet management systems including M2M applications and wireless modems. Thomas has over 15 years’ experience in product design and electronics engineering. He holds a Master of Science degree from KTH Royal Institute of Technology.</p>',
            '<h2>Lars Lindqvist, Independent Board Member</h2>',
            '<p><a href="mailto:lars.linqvist@neonode.com" class="inline-anchor">lars.linqvist@neonode.com</a><br>',
            'Lars Lindqvist is the Chief Financial Officer of Mankato Investments AG, which he joined in 2005. Prior to that, he was the Chief Financial Officer at Microcell Oy, which he joined in 2002. He previously served as Chief Financial Officer of Ericsson Mobile Phones from 1995 to 2002. At Ericsson, Lars was also responsible for the IS/IT function as well as the implementation of a new business model. He served at Ericsson Group as a Manager from 1982. In addition, Lars served as Unit Controller at Ericsson Information System from 1986, where he was directly involved in the divestment of the whole business to Nokia 1991. He served as Business Area Controller of Ericsson Radio System, American standard 1991. He serves as Director of Nanjing Scandinavian Industrial Campus AB. Lars holds a Master of Finance degree at Uppsala University, Sweden.</p>',
            '<h2>Mats Dahlin, Independent Board Member</h2>',
            '<p><a href="mailto:mats.dahlin@neonode.com" class="inline-anchor">mats.dahlin@neonode.com</a><br>',
            'Mats Dahlin has been Senior Advisor of Neonode Technologies AB, a subsidiary of Neonode Inc. since December 2009. He has been a Director of Neonode Inc. since November 17, 2011. Mats joined Ericsson in 1980 and served as Vice President, Executive Vice President and General Manager of several Business Units within Ericsson in Europe, the Middle East and Africa. He served as President for Ericsson Enterprise AB from 2004 to May 2005. Mats served as President of Ericsson Radio Systems AB from 1998 and Head of Business Segment Network Operators responsible for the Mobile Systems Division from 2000. In 1984, he started at Cellular Systems Group as Regional Manager Cellular Systems for the Far East and Oceania. Mats also served in senior positions such as Vice President and Director of Cellular Business, Ericsson Canada, and Vice President of Project Management and Materials and General Manager of Business Unit Mobile Data Systems at Ericsson US. Mats served as In Charge of Market Operations, the Americas within the Business Unit GSM, NMT and TACS from 1994 to 1997. He served as Head of Market Area EMEA (Europe, Middle East and Africa) at Ericsson from 2001. He serves as the Chairman of the Board at Teligent Telecom AB and has been Chairman of Apptoo AB since April 27, 2007. He has additionally served as the Chairman of the Board of Appgate Network Security AB. From June 2005, he served as an Advisor to venture capital and private equity companies. Mats served as Chairman and Director of PacketFront Sweden AB. Mats holds a Degree in Engineering and a B.Sc. degree in Business Administration from Stockholm University.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    career: {
        intro: function ( id ) {
            var content = [
                '<h1>Join the world’s leading developer of Optical Touch Technology!</h1>',
                '<p>Neonode develops technology used in touch screen devices such as mobile smart phones, eReaders, Tablet PCs, Navigation devices and more. Neonode has revolutionized the touch screen market with zForce®, our optical touch screen technology.</p>',
                '<p>Neonode develops innovative technologies used in touch applications and devices such as mobile smart phones, eReaders, Tablet PCs, Navigation devices and many more. Neonode has revolutionized the touch technology market for reflective displays with its zForce® optical touch screen technology.</p>',
                '<h2>In 2012 we will double our efforts, increase our team and expand our market, and we want you to be a part of that success!</h2>',
                '<p>At Neonode we work closely in teams to develop the markets most advanced optical touch technology for customers and partners. You will be working in an informal and creative environment, where your expertise and skills are highly valued as well as you as a person. You will be working with some of the leading professionals in their field, that will inspire you to develop and step up your game &#8211; we are sure that you will fit right in!</p>',
                '<p>In a fast paced workplace, located in five international offices, we create innovation every day. We encourage diversity in our teams, and value applications from all parts of the world -<b> the open positions are located at our Stockholm and CA offices.</b></p>',
                '<p>Most of our open positions are in engineering and technical project management, as our market offer consists of a winning, high performing technology. R&amp;D, engineering and industrial design are examples of what competence we are looking for.</p>',
                '<h2>For open positions in Stockholm</h2>',
                '<p>Apply by sending your application and CV to Niklas Kvist, Head of R&amp;D <a class="inline-anchor" href="mailto:niklas.kvist@neonode.com">niklas.kvist@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>',
                '<h2>For open positions in Santa Clara, CA, U.S.</h2>',
                '<p>Apply by sending your application and CV to Douglas Young, VP Sales North-America <a class="inline-anchor" href="mailto:douglas.young@neonode.com">douglas.young@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        syseng: function ( id ) {
            var content = [
                '<h1>System engineers with test experience</h1>',
                '<p>In this position you will be part of a development team which includes software developers, electronics designers, mechanical engineers, electrical engineers and other system engineers. You will be working at the leading edge of optical touch screen technology. You will work mainly on customer projects with products which are existing and/or in development for consumer markets. There will be customer contact as both a specialist and for technical sales support. It is important that you be familiar with and enjoy working in an entrepreneurial environment. Neonode has a small and flexible organization with short decision paths. We are a multi-national company with international customers so travel will be required.</p>',
                '<h2>Desired Skills &amp; Experience</h2>',
                '<p>A well-organized person with a good sense for details and with test experience.<br>',
                'Broad engineering background with good knowledge of at least one of the<br>',
                'following areas:</p>',
                '<ul>',
                '<li>Electronics</li>',
                '<li>Low level SW (C++)</li>',
                '<li>Mechanical design</li>',
                '<li>Optics</li>',
                '</ul>',
                '<h2>Personal profile</h2>',
                '<ul>',
                '<li>A team player who likes to be part of a small team with quick decision-making</li>',
                '<li>Unpretentious attitude</li>',
                '<li>Driven and self-propelled</li>',
                '<li>Desirable that the person has a broad experience in technology development</li>',
                '</ul>',
                '<h2>For open positions in Stockholm</h2>',
                '<p>Apply by sending your application and CV to <a class="inline-anchor" href="mailto:niklas.kvist@neonode.com">niklas.kvist@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>',
                '<h2>For open positions in Santa Clara, CA, U.S.</h2>',
                '<p>Apply by sending your application and CV to Douglas Young, VP Sales North-America <a class="inline-anchor" href="mailto:douglas.young@neonode.com">douglas.young@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        electroeng: function ( id ) {
            var content = [
                '<h1>Electro-mechanical engineers to Stockholm and Santa Clara, CA, U.S</h1>',
                '<p>In this position you will be part of a development team which includes software developers, electronics designers, other mechanical engineers, electrical engineers and system engineers who work in an international environment. You will be working at the leading edge of optical touch screen technology and work on customer projects with products which are existing and/or in development for consumer markets. There will be customer contact as both a specialist and for technical sales support. It is important that you be familiar with and enjoy working in an entrepreneurial environment. Neonode has a small and flexible organization with short decision paths. We are a multi-national company with international customers so travel will be required.</p>',
                '<h2>Desired Skills &amp; Experience</h2>',
                '<ul>',
                '<li>Specialized in designing plastic parts with optical performance</li>',
                '<li>SolidWorks experience</li>',
                '</ul>',
                '<h2>Personal profile</h2>',
                '<ul>',
                '<li>A team player who likes to be part of a small team with quick decision-making</li>',
                '<li>Unpretentious attitude</li>',
                '<li>Driven and self-propelled</li>',
                '<li>Desirable that you have experience in technology development</li>',
                '</ul>',
                '<h2>For open positions in Stockholm</h2>',
                '<p>Apply by sending your application and CV to <a class="inline-anchor" href="mailto:niklas.kvist@neonode.com">niklas.kvist@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>',
                '<h2>For open positions in Santa Clara, CA, U.S.</h2>',
                '<p>Apply by sending your application and CV to Douglas Young, VP Sales North-America <a class="inline-anchor" href="mailto:douglas.young@neonode.com">douglas.young@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        optoeng: function ( id ) {
            var content = [
                '<h1>Opto-mechanical engineer</h1>',
                '<p>In this position you will be part of a development team which includes software developers, electronics designers, mechanical engineers, electrical engineers, system engineers and other opto-mechanical engineers who work in an international environment. You will be working at the leading edge of optical touch screen technology and work on customer projects with products which are existing and/or in development for consumer markets. There will be customer contact as both a specialist and for technical sales support. It is important that you be familiar with and enjoy working in an entrepreneurial environment. Neonode has a small and flexible organization with short decision paths. We are a multi-national company with international customers so travel will be required.</p>',
                '<h2>Desired Skills &amp; Experience</h2>',
                '<ul>',
                '<li>In depth knowledge of plastic injection molding and tooling</li>',
                '<li>SolidWorks experience</li>',
                '</ul>',
                '<h2>Personal profile</h2>',
                '<ul>',
                '<li>A team player who likes to be part of a small team with quick decision-making</li>',
                '<li>Unpretentious attitude</li>',
                '<li>Driven and self-propelled</li>',
                '<li>Desirable that you have experience in technology development</li>',
                '</ul>',
                '<p>Apply by sending your application and CV to <a class="inline-anchor" href="mailto:niklas.kvist@neonode.com">niklas.kvist@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        mecheng: function ( id ) {
            var content = [
                '<h1>Mechanical engineer</h1>',
                '<p>In this position as Mechanical Engineer, you will be part of a development team which includes software developers and electronics designers. You will be working at the leading edge of optical touch screen technology. You will work mainly on customer projects with products which are existing and/or in development for consumer markets. There will be customer contact as both a specialist and for technical sales support. It is important that you be familiar with and enjoy working in an entrepreneurial environment. Neonode has a small and flexible organization with short decision paths. We are a multi-national company with international customers so travel will be required.</p>',
                '<h2>We would like you to have SolidWorks experience and any of the following skills</h2>',
                '<ul>',
                '<li>Master of Science in Mechanical Engineering</li>',
                '<li>2-5 years work experience either in a consulting company or consumer device company or similar related experience</li>',
                '<li>Experience in plastics design</li>',
                '<li>Good understanding of the relationship between product design and manufacturing</li>',
                '<li>Experience and comfort with external customer contacts</li>',
                '<li>English (spoken and written)</li>',
                '<li>Superior experience of plastic injection molding and tooling</li>',
                '<li>Specialized in designing plastic parts with optical performance</li>',
                '<li>Experience in Electro-mechanical design</li>',
                '</ul>',
                '<h2>Personal profile</h2>',
                '<ul>',
                '<li>A team player who likes to be part of a small team with quick decision-making</li>',
                '<li>Unpretentious attitude</li>',
                '<li>Driven and self-propelled</li>',
                '<li>Desirable that the person has a broad experience in technology development</li>',
                '</ul>',
                '<h2>For open positions in Stockholm</h2>',
                '<p>Apply by sending your application and CV to <a class="inline-anchor" href="mailto:niklas.kvist@neonode.com">niklas.kvist@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>',
                '<h2>For open positions in Santa Clara, CA, U.S.</h2>',
                '<p>Apply by sending your application and CV to Douglas Young, VP Sales North-America <a class="inline-anchor" href="mailto:douglas.young@neonode.com">douglas.young@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        projectmanager: function ( id ) {
            var content = [
                '<h1>Technical Project Manager</h1>',
                '<p>You are a dedicated person with the relevant education and experience from working with high-end innovative and complex tech development projects. In this position you will be part of and lead a development team which includes software developers, electronics designers, mechanical engineers, electrical engineers and system engineers. You will be working at the leading edge of optical touch screen technology. You will work mainly on customer projects with products which are existing and/or in development for consumer markets. There will be customer contact. It is important that you be familiar with and enjoy working in an entrepreneurial environment. Neonode has a small and flexible organization with short decision paths. We are a multi-national company with international customers so travel will be required.</p>',
                '<h2>Desired Skills &amp; Experience</h2>',
                '<ul>',
                '<li>Experience of managing high tech projects in a fast paced environment</li>',
                '<li>Engineering background (preferable mechanical)</li>',
                '</ul>',
                '<h2>Personal profile</h2>',
                '<ul>',
                '<li>A team player with leadership skills who likes to be part of a small team with quick decision-making</li>',
                '<li>Unpretentious attitude</li>',
                '<li>Driven and self-propelled</li>',
                '<li>Desirable that the person has a broad experience in technology development</li>',
                '</ul>',
                '<p>Apply by sending your application and CV to <a class="inline-anchor" href="mailto:niklas.kvist@neonode.com">niklas.kvist@neonode.com</a>. Name your application with the position you are applying for. We want you now, so act today!</p>'
            ];

            return this.wrapper( id, content.join( '' ) );
        },
        wrapper: function ( activePage, innerContent ) {
            var auxActivePage = activePage.split( '-' ),
                response = [
                    '<div class="content-wrapper1">',
                    GeneratorModel.submenu( [
                        {
                            label: 'Sub-pages',
                            activeItem: auxActivePage[0] + '-' + auxActivePage[1],
                            items: [
                                {
                                    label: 'Corporate information',
                                    page: 'aboutPage-corporate'
                                },
                                {
                                    label: 'Background',
                                    page: 'aboutPage-background',
                                    bpMobile: true
                                },
                                {
                                    label: 'Career',
                                    page: 'aboutPage-career',
                                    bpTablet: true
                                },
                                {
                                    label: 'Management',
                                    page: 'aboutPage-management'
                                },
                                {
                                    label: 'Board of Directors',
                                    page: 'aboutPage-board'
                                }
                            ]
                        },
                        {
                            label: 'Open positions',
                            activeItem: activePage,
                            items: [
                                {
                                    label: 'System Engineers',
                                    page: 'aboutPage-career-syseng'
                                },
                                {
                                    label: 'Electro-mechanical Engineer',
                                    page: 'aboutPage-career-electroeng',
                                    bpMobile: true
                                },
                                {
                                    label: 'Opto-Mechanical Engineer',
                                    page: 'aboutPage-career-optoeng',
                                    bpTablet: true
                                },
                                {
                                    label: 'Mechanical Engineer',
                                    page: 'aboutPage-career-mecheng',
                                    bpMobile: true
                                },
                                {
                                    label: 'Technical Project Manager',
                                    page: 'aboutPage-career-projectmanager'
                                }
                            ]
                        }
                    ] ),
                    '<div class="content-wrapper2">',
                    innerContent,
                    '</div><div class="content-wrapper3"></div></div>'
                ];
            return response.join( '' );
        }
    },
    wrapper: function ( activePage, innerContent ) {
        var response = [
            '<div class="content-wrapper1">',
            GeneratorModel.submenu( [
                {
                    label: 'Sub-pages',
                    activeItem: activePage,
                    items: [
                        {
                            label: 'Corporate information',
                            page: 'aboutPage-corporate'
                        },
                        {
                            label: 'Background',
                            page: 'aboutPage-background',
                            bpMobile: true
                        },
                        {
                            label: 'Career',
                            page: 'aboutPage-career',
                            bpTablet: true
                        },
                        {
                            label: 'Management',
                            page: 'aboutPage-management'
                        },
                        {
                            label: 'Board of Directors',
                            page: 'aboutPage-board'
                        }
                    ]
                }
            ] ),
            '<div class="content-wrapper2">',
            innerContent,
            '</div><div class="content-wrapper3"></div></div>'
        ];
        return response.join( '' );
    }

} );
