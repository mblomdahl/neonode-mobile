Ext.define( 'Neonode.model.ProductsModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'ProductsModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel'
    ],

    intro: function ( id ) {
        return this.zforce( id );
    },
    zforce: function ( id ) {
        var content = [
            '<h1>zForce® – Versatile Patented Touch Technology</h1>',
            '<p>Neonode\'s touch technology zForce® is unequalled and award-winning. Get the generous feature summary here - let your imagination or our support team do the rest!</p>',
            '<p>In terms of user input, zForce® combines or exceeds the advantages of resistive and capacitive. It requires zero touch force, has high sensitivity, and is insensitive to electrical interference. For pen writing, it has no parallax, the highest frame rate and resolution, and may be used with any object, rather than a special stylus, that includes gestures, multi-touch, sweeps and much more. Since nothing is layered on top of the display, it provides 100% optical transparency, which improves image quality, reduces glare, and improves battery life (because the backlight needs less power to achieve desired brightness).</p>',
            '<p><b>zForce</b> is the only viable touch solution that operates on the new revolutionary reflective display panels offering paper-like reading experience in almost any ambient lighting condition. zForce® is currently being integrated into a variety of smartphones, e-Readers, printer products, automotive applications, tablet devices and other touch enabled devices – it offers the highest level of touch functionality at the lowest cost.</p>',
            '<h2>zForce works with gloves!</h2>',
            '<p><b>zForce</b> can be applied on any flat surface such as Liquid Crystal Display (LCD), Organic Light Emitting Diode Displays(OLED), Electronic Paper Displays (EPD), Mirasol, Plasticlogic or Mouse Pad and can be incorporated into all types of devices, enabling touch detection for any type of object (finger, pen, glove). In addition the zForce touch solution has a remarkably low total weight and building height, thus enabling greater industrial design flexibility.<br>',
            '<br>* <i>Many solutions require toughened glass to protect LCD, and in the devices zForce® advances again as with zForce there is no requirement for air gap between LCD and ITO covered glass, therefor the display visibility is far better with zForce than any other touch technology.</i><br>',
            '<br>** <i>With zForce® enabled touch screens there is no requirement for glass or plastic film overlay on top of the display window, providing a 100% clear viewing experience.</i></p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    nn1001: function ( id ) {
        var content = [
            '<h1>NN1001 – Developed for the next generation of automotive touch displays</h1>',
            '<p>The NN1001 ASIC adds new features as no overlay, low latency pen tracking, proximity sensing and object-size measurement.</p>',
            '<h2>With Neonode AlwaysONTM technology! Works with gloves too!</h2>',
            '<p>The NN1001 is designed to reduce bill of material (BOM) cost, increase performance and add superior functionality to small &amp; medium sized touch enabled devices. The NN1001 competes with low cost resistive touch while outperforming capacitive touch solutions by combining the best from resistive (low cost, pen writing) and capacitive solutions (multi touch, and gestures tracking).</p><ul>',
            '<li>The NN1001 has a stunning scanning speed of 1000 Hz (latency down to 1ms) and consumes less than 1mW at 100Hz.</li>',
            '<li>The NN1001 tracks any high-speed multi-touch gesture with any object (finger, <b>gloved finger</b> and passive pens) with high accuracy.</li>',
            '<li>The NN1001 connects to any microcontroller or application processor with a high speed SPI interface. The controller works in single or multiple configurations supporting screen sizes up to 20 inch.</li>',
            '<li>The NN1001 supports advanced power management and implements he Neonode Registered Trademark AlwaysONTM technology where the touch is active even when the device is in sleep- or off mode.</li></ul>',
            '<p><a class="menu-item" href="#contactPage">Contact us for more information and demonstrations</a>.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    mobile: function ( id ) {
        var content = [
            '<h1>Mobile</h1>',
            '<p>Neonode® licenses zForce® which is the markets most versatile optical touch technology, delivering peak performance at an incredibly low cost. Our offer includes design engineering services and unique conceptual adaptations and support for each and every customer and partner.</p>',
            '<p>High-performance capacitive touch screens are expensive while resistive touch screens are low performing. We solve this challenge with zForce®, our groundbreaking touch technology innovation suitable for mobile and virtually all other hand held devices – a state-of-the-art solution for OEMs.<br>',
            '<br>zForce®:<ul>',
            '<li>Runs on all screen types and provides a crystal-clear viewing experience in any lighting condition, even bright sunlight.</li>',
            '<li>Supports high-resolution pen writing in combination with finger navigation that includes, e.g., gestures, multi-touch, and sweeps.</li>',
            '<li>Does not require overlay on top of the display window.</li>',
            '<li>Can be applied to any flat surface.</li>',
            '<li>Enables touch detection for any object and greater industrial design flexibility</li>',
            '</ul><p>zForce® features benefit Neonode’s customers by facilitating the creation of top-quality technology  with outstanding performance. Reduced production costs. Faster time to market. High market competitiveness.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    tablet: function ( id ) {
        var content = [
            '<h1>Tablets and e-readers</h1>',
            '<h2>The market\'s leading optical touch technology</h2>',
            '<p>zForce® drastically reduces cost and shortens the time-to-market for equipment- and device manufacturers and is found in a number of large brand products.</p>',
            '<p>High-performance capacitive touch screens cost a lot to manufacture, while low-performance resistive touch screen are cheaper to make. Thanks to extensive experience and expertise, Neonode has solved this production-cost problem by developing  zForce®—the markets most innovative optical touch technology that’s customized for tablets and e-readers.</p>',
            '<p>zForce is the world’s most-used optical touch technology for e-readers today. It is the first choice for tablet and e-reader manufacturers because it:</p><ul>',
            '<li>Operates on all screen types and provides a crystal-clear viewing experience in any lighting condition, even bright sunlight.</li>',
            '<li>Runs on EDP (Electronic Paper Displays) that creates a readable experience close to ink on paper because it reflects light like ordinary paper.</li>',
            '<li>Operates on LCD (Liquid Crystal Displays).</li>',
            '<li>Supports high-resolution pen writing in combination with finger navigation that includes, e.g., gestures, multi-touch, and sweeps.</li>',
            '<li>Provides a 100% clear viewing experience—unlike traditional resistive and capacitive touch screens.</li>',
            '<li>Can be applied to any flat surface.</li></ul>',
            '<p>Enables:</p><ul>',
            '<li>Touch detection for any object.',
            '<li>A smooth touch experience with no necessary pressure for touch detection.',
            '<li>Greater industrial design flexibility for equipment and device manufacturers.</ul>',
            '<p>zForce® features’ benefit Neonode’s customers by facilitating the creation of top-quality products, at lower production costs which enable faster time to market.</p>',
            '<p>Neonode’s solution for tablets and e-readers is already found in large brand products on a global market.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    automotive: function ( id ) {
        var content = [
            '<h1>Automotive</h1>',
            '<h2>The Automotive Environment Poses Unique Challenges</h2>',
            '<p>Neonode® licenses zForce® which is the markets most versatile optical touch technology, delivering peak performance at an incredibly low cost. Our offer includes design engineering services and unique conceptual adaptations and support for each and every customer and partner.</p>',
            '<p>High-performance capacitive touch screens are expensive while resistive touch screens are low performing. We solve this challenge with zForce®, our groundbreaking touch technology innovation.</p>',
            '<p>Touch interface displays in vehicles must operate in a wide range of ambient lighting and temperature conditions. A resistive screen may be operated with gloves on, but the membrane layer reduces transparency and can cause glare. The zForce® solution provides a brighter, more readable display, with a full operating temperature range, that can easily be used while wearing gloves.</p>',
            '<p>zForce is the most cost-effective, high-performing, touch technology for automotive manufacturers because it:</p><ul>',
            '<li>Operates on all screen types and provides a crystal-clear viewing experience in any lighting condition, including sunlight.</li>',
            '<li>Provides a 100% clear viewing experience—it’s free from reflection and parallax effects.</li>',
            '<li>Supports finger navigation – even with gloves – that includes gestures, multi-touch, and sweeps.</li>',
            '<li>Can be applied to any flat surface, e.g., liquid crystal display, organic light-emitting diode displays, and electronic paper displays.</li>',
            '<li>Has a remarkably low total weight and building height.</li>',
            '<li>Enables touch detection for any object and greater industrial design flexibility.</li></ul>',
            '<p>Here are a few benefits from Neonode’s zForce touch technology: High quality and performance. Reduced production costs. Faster time to market. High market competitiveness.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    office: function ( id ) {
        var content = [
            '<h1>Printers and office equipment</h1>',
            '<h2>Improved performance for the same cost as resistive</h2>',
            '<p>zForce® features’ benefit Neonode’s customers by facilitating the creation of top-quality products, at lower production costs which enable faster time to market.</p>',
            '<p>Photo printers and combination printer/scanner/fax machines typically require feature-rich menus and settings, and OEMs have increasingly replaced mechanical buttons with resistive touch screen displays. zForce offers an improved user experience, with brighter display, ultra light touch, and support for gestures (such as swipe to access menus and screens), all without increasing cost.</p>',
            '<p>zForce is the first choice for printers and other office equipment manufacturers because it:</p><ul>',
            '<li>Operates on all screen types and provides a crystal-clear viewing experience in any lighting condition, even bright sunlight.</li>',
            '<li>Runs on EDP (Electronic Paper Displays) that creates a readable experience close to ink on paper because it reflects light like ordinary paper.</li>',
            '<li>Operates on LCD (Liquid Crystal Displays).</li>',
            '<li>Supports high-resolution pen writing in combination with finger navigation that includes, e.g., gestures, multi-touch, and sweeps.</li>',
            '<li>Provides a 100% clear viewing experience—unlike traditional resistive and capacitive touch screens.</li>',
            '<li>Can be applied to any flat surface.</li>',
            '<li>Touch detection for any object.</li>',
            '<li>A smooth touch experience with no necessary pressure for touch detection.</li>',
            '<li>Greater industrial design flexibility for equipment and device manufacturers.</li></ul>',
            '<p>zForce® features’ benefit Neonode’s customers by facilitating the creation of top-quality products, at lower production costs which enable faster time to market.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    home: function ( id ) {
        var content = [
            '<h1>Home electronics</h1>',
            '<p>Neonode® licenses zForce® which is the markets most versatile optical touch technology, delivering peak performance at an incredibly low cost. Our offer includes design engineering services and unique conceptual adaptations and support for each and every customer and partner.</p>',
            '<p>Machines in the kitchen and laundry room are still mostly controlled by mechanical buttons, dials and membrane switches. New designs can use zForce with or without an underlying display. For example, touch sensitive buttons can be achieved by placing the light guide around a pre-printed array.  A touch panel can include illumination without a display. A feature-rich device like a high-end dryer can be made much more streamlined and userfriendly with a touch display. There are endless possibilities.</p>',
            '<p>zForce is the most advanced touch technology for home appliances because, among other things, it:</p><ul>',
            '<li>Operates on all screen types and provides a crystal-clear viewing experience in any lighting condition, even bright sunlight.</li>',
            '<li>Supports high-resolution with finger navigation – even with gloves – that includes gestures, multi-touch, and sweeps.</li>',
            '<li>Can be applied to any flat surface.</li></ul>',
            '<p>Enables:</p><ul>',
            '<li>Touch detection for any type of object.</li>',
            '<li>Greater industrial design flexibility.</li></ul>',
            '<p>How do these functions and features benefit Neonode’s customers? High quality and performance. Reduced production costs. Faster time to market. High market competitiveness.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    toys: function ( id ) {
        var content = [
            '<h1>Games and toys</h1>',
            '<p>Neonode® licenses zForce® which is the markets most versatile optical touch technology, delivering peak performance at an incredibly low cost. Our offer includes design engineering services and unique conceptual adaptations and support for each and every customer and partner.</p>',
            '<p>Busy consumers in a busy world know what they want. They´re technologically savvy and expect sophisticated functions, high quality and usability. They expect the best. Neonode®’s market offer is durable touch solutions based on our proprietary touch technology zForce® which meet these high requirements.',
            '<p>High-performance capacitive touch screens are expensive while resistive touch screens are low performing. We solve this challenge with zForce®, our groundbreaking touch technology.</p>',
            '<p>zForce is the optimal touch technology for game and toy manufacturers because it:</p><ul>',
            '<li>Operates on all screen types and provides a crystal-clear viewing experience in any lighting conditions, even bright sunlight.</li>',
            '<li>Supports high-resolution pen writing in combination with finger navigation that includes, e.g., gestures, multi-touch, and sweeps.</li></ul>',
            '<p>Can be applied to any flat surface.</li></ul>',
            '<p>Enables:</p><ul>',
            '<li>Touch detection for any object.</li>',
            '<li>Greater industrial design flexibility.</li></ul>',
            '<p>zForce functions and features enable these and other benefits:  High quality and performance. Reduced production costs. Faster time to market. High market competitiveness.</p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    wrapper: function ( activePage, innerContent ) {
        var response = [
            '<div class="content-wrapper1">',
            GeneratorModel.submenu( [
                {
                    label: 'Technologies',
                    activeItem: activePage,
                    items: [
                        {
                            label: 'zForce®',
                            page: 'productsPage-intro'
                        },
                        {
                            label: 'NN1001',
                            page: 'productsPage-nn1001'
                        }
                    ]
                },
                {
                    label: 'Markets',
                    activeItem: activePage,
                    items: [
                        {
                            label: 'Mobile',
                            page: 'productsPage-mobile'
                        },
                        {
                            label: 'Tablets and e-readers',
                            page: 'productsPage-tablet'
                        },
                        {
                            label: 'Automotive',
                            page: 'productsPage-automotive',
                            bpMobile: true
                        },
                        {
                            label: 'Home electronics',
                            page: 'productsPage-home',
                            bpTablet: true
                        },
                        {
                            label: 'Printers and office equipment',
                            page: 'productsPage-office',
                            bpMobile: true
                        },
                        {
                            label: 'Games and toys',
                            page: 'productsPage-toys'
                        },
                    ]
                }
            ] ),
            '<div class="content-wrapper2">',
            innerContent,
            '</div><div class="content-wrapper3"></div></div>'
        ];
        return response.join( '' );
    }
} );
