Ext.define( 'Neonode.model.FutureConceptModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'FutureConceptModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel'
    ],

    intro: function ( id ) {
        var response = [
            '<div class="content-wrapper1">',
            GeneratorModel.submenu( [
                {
                    label: 'Sub-pages',
                    activeItem: 'futureConceptPage-intro',
                    items: [
                        {
                            label: 'Introduction',
                            page: 'futureConceptPage-intro'
                        },
                        {
                            label: 'Concept Gallery Slideshow',
                            page: 'futureConceptPage-gallery'
                        }
                    ]
                }
            ] ),
            '<div class="content-wrapper2">',
            '<h1>Discover our Concept Solutions</h1>',
            '<p>This is a presentation of several unequalled concepts based on Neonode zForce® - our patented and robust touch screen technology. We have developed zForce to match virtually all types of hand-held touch screen enabled devices. Mobile phones, e-readers, tablets, touch screens in cars, games and toys. We invite you to be inspired by our conceptual world for the new generation of touch screens.</p>',
            '<h2>zForce®</h2>',
            '<p>zForce® is the only viable touch screen solution that operates on the new revolutionary reflective display panels offering paper-like reading experience in almost any ambient lighting condition. zForce® is currently being integrated into a variety of mobile phones, e-Readers, printer products, automotive applications, tablet devices and other touch enabled devices.</p>',
            '<h2>Neonode concept gallery</h2>',
            '<p><a href="#futureConceptPage-gallery" class="submenu-item">Visit the Concept Gallery Slideshow.</a></p>',
            '</div><div class="content-wrapper3"></div>',
            '</div>'
        ];

        return response.join( '' );
    },
    galleryContent: [
        {
            image: 'resources/images/concept1.png',
            header: '<h3>Sweeped Frame Tablet</h3>',
            description: '<p>Meet the conceptual Sweeped Frame Tablet and take the opportunity to enjoy a whole new way to experience touch design. In this concept our technology has been merged with shaped plastic that seemlessly meet the display. The lightguide – an integral part of our technology – is elegantly hidden behind the curves. For usability and comfort. The Swedish design tradition and winter forests inspire this conceptual tablet. Simple, stylish and original.</p>'
        },
        {
            image: 'resources/images/concept2.png',
            header: '<h3>High End Phone</h3>',
            description: '<p>A higher end High End Phone. This concept displays a straight lightguide resulting in a device for a demanding consumer. The intelligence is the brushed metal on the sides that seemlessly hold the plastic together. We believe this concept mobile phone really deserve an inductive charge, not to disturb the clean and seemingly effortless design. Speakers and microphone are, for the same reason, integrated at the bottom of the device. A sharp design, with straight draws where soft curves and classic speaker pattern bring the modern, yet classic new generation mobile phone to life.</p>'
        },
        {
            image: 'resources/images/concept3.png',
            header: '<h3>Sweeped Side Frame Phone</h3>',
            description: '<p>Side by side Sweeped Side Frame Phone. A metal frame in one whole, uninterrupted piece - with a thin layer of more polished metal. Inside is the lightguide. The idiom of this concept mobile phone is one that cannot be questioned. The use of a radius that heads in different directions gives it an interesting character – a design for the bold. A softer, smoother concept with the speakers in chamfer edges rests comfortably in any hand.</p>'
        },
        {
            image: 'resources/images/concept4.png',
            header: '',
            description: ''
        },
        {
            image: 'resources/images/concept5.png',
            header: '',
            description: ''
        },
        {
            image: 'resources/images/concept6.png',
            header: '',
            description: ''
        },
        {
            image: 'resources/images/concept7.png',
            header: '',
            description: ''
        },
        {
            image: 'resources/images/concept8.png',
            header: '',
            description: ''
        }
    ],
    gallery: function ( id ) {
        var itemArray = [];
        for ( var tmp, content = this.galleryContent, i = 0, j = content.length;
              i < j;
              i++ ) {
            tmp = [
                '<img src="' + content[i].image + '" class="gallery-item"><div class="gallery-item">',
                content[i].header,
                content[i].description,
                '<br><br></div>'
            ].join( '' );
            itemArray.push( {
                scrollable: 'vertical',
                html: tmp
            } );
        }

        return itemArray;
    }
} );
