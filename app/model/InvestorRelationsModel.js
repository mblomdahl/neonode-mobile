Ext.define( 'Neonode.model.InvestorRelationsModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'InvestorRelationsModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    intro: function () {
        var response = [
            '<div class="content-wrapper1"><div class="content-wrapper2">',
            '<h1>Investor relations</h1>',
            '<p>Our Investor Relations pages are designed to provide all investors, share holders and the capital markets with reliable, equable and precise information about our company.<br>',
            '<br>Redirect to neonode.com goes here.</p>',
            '</div><div class="content-wrapper3"></div>',
            '</div>'
        ];

        return response.join( '' );
    }
} );
