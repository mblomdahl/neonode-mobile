Ext.define( 'Neonode.model.ContactModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'ContactModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel'
    ],

    headoffice: function ( id ) {
        var content = [
            '<h1>Head office</h1>',
            '<p>Let\'s get in touch! Forming partnerships, networking and maintaining good client relationships is part of what we do. Welcome to contact us.</p>',

            '<h3>Neonode, Inc. / Neonode&nbsp;Technologies&nbsp;AB<br>',
            'Linnégatan 89<br>SE-115 23 Stockholm, Sweden</h3>',

            '<p>Phone: <a href="tel:+4686671717" class="inline-anchor">+46 8 667-1717</a><br>',
            'Email: <a href="mailto:sales@neonode.com" class="inline-anchor">sales@neonode.com</a> | <a href="mailto:info@neonode.com" class="inline-anchor">info@neonode.com</a><br>',

            '<br><b>Thomas Eriksson</b>, CEO<br>',
            'Phone: <a href="tel:+4686671717" class="inline-anchor">+46 8 667-1717</a><br>',
            'Email: <a href="mailto:thomas.eriksson@neonode.com" class="inline-anchor">thomas.eriksson@neonode.com</a><br>',

            '<br><b>David Brunton</b>, CFO<br>',
            'Phone: <a href="tel:+19257680620" class="inline-anchor">+1 925 768-0620</a><br>',
            'Email: <a href="mailto:david.brunton@neonode.com" class="inline-anchor">david.brunton@neonode.com</a><br>',

            '<br><b>Annica Englund</b>, Marketing Director<br>',
            'Phone: <a href="tel:+46760360781" class="inline-anchor">+46 760 36-0781</a><br>',
            'Email: <a href="mailto:annica.englund@neonode.com" class="inline-anchor">annica.englund@neonode.com</a></p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    europe: function ( id ) {
        var content = [
            '<h1>Europe/Global</h1>',
            '<p>Let\'s get in touch! Forming partnerships, networking and maintaining good client relationships is part of what we do. Welcome to contact us.<br>',
            '<br><b>Remo Behdasht</b>, VP Sales<br>',
            'Phone: <a href="tel:+31610376373" class="inline-anchor">+31 610 37-6373</a><br>',
            'Email: <a href="mailto:remo.behdasht@neonode.com" class="inline-anchor">remo.behdasht@neonode.com</a><br>',

            '<br><b>Gunnar Fröjdh</b>, VP Sales<br>',
            'Email: <a href="mailto:gunnar.frojdh@neonode.com" class="inline-anchor">gunnar.frojdh@neonode.com</a></p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    usa: function ( id ) {
        var content = [
            '<h1>U.S.</h1>',
            '<p>Let\'s get in touch! Forming partnerships, networking and maintaining good client relationships is part of what we do. Welcome to contact us.</p>',

            '<h3>U.S. Office Neonode Inc.<br>2700 Augustine Drive<br>Santa Clara, CA 95054-2904</h3>',

            '<p><b>Douglas Young</b>, VP General Manager Americas<br>',
            'Phone: <a href="tel:+14082028477" class="inline-anchor">+1 408 202-8477</a><br>',
            'Email: <a href="mailto:douglas.young@neonode.com" class="inline-anchor">douglas.young@neonode.com</a><br>',

            '<br><b>Carlos Torreblanca</b>, Director of Sales Americas<br>',
            'Phone: <a href="tel:+14083985892" class="inline-anchor">+1 408 398-5892</a><br>',
            'Email: <a href="mailto:carlos.torreblanca@neonode.com" class="inline-anchor">carlos.torreblanca@neonode.com</a><br>',

            '<br><b>David Brunton</b>, CFO<br>',
            'Phone: <a href="tel:+19257680620" class="inline-anchor">+1 925 768-0620</a><br>',
            'Email: <a href="mailto:david.brunton@neonode.com" class="inline-anchor">david.brunton@neonode.com</a></p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    asia: function ( id ) {
        var content = [
            '<h1>Asia</h1>',
            '<p>Let\'s get in touch! Forming partnerships, networking and maintaining good client relationships is part of what we do. Welcome to contact us.<br>',

            '<br><b>Richard Kim</b>, VP Sales<br>',
            'Phone: <a href="tel:+821056122270" class="inline-anchor">+82 10 5612-2270</a><br>',
            'Email: <a href="mailto:richard.kim@neonode.com" class="inline-anchor">richard.kim@neonode.com</a><br>',

            '<br><b>Remo Behdasht</b>, VP Sales Japan<br>',
            'Phone: <a href="tel:+31610376373" class="inline-anchor">+31 610 37-6373</a><br>',
            'Email: <a href="mailto:remo.behdasht@neonode.com" class="inline-anchor">remo.behdasht@neonode.com</a><br>',

            '<br><b>Johan Eriksson</b>, VP Sales<br>',
            'Phone: <a href="tel:+46703728217" class="inline-anchor">+46 70 372-8217</a><br>',
            'Email: <a href="mailto:johan.eriksson@neonode.com" class="inline-anchor">johan.eriksson@neonode.com</a></p>'
        ];

        return this.wrapper( id, content.join( '' ) );
    },
    wrapper: function ( activePage, innerContent ) {
        var response = [
            '<div class="content-wrapper1">',
            GeneratorModel.submenu( [
                {
                    label: 'Offices',
                    activeItem: activePage,
                    items: [
                        {
                            label: 'Head office',
                            page: 'contactPage-headoffice'
                        },
                        {
                            label: 'Global/Europe',
                            page: 'contactPage-europe'
                        },
                        {
                            label: 'U.S.',
                            page: 'contactPage-usa'
                        },
                        {
                            label: 'Asia',
                            page: 'contactPage-asia'
                        }
                    ]
                }
            ] ),
            '<div class="content-wrapper2">',
            innerContent,
            '</div><div class="content-wrapper3"></div></div>'
        ];
        return response.join( '' );
    }
} );
