Ext.define( 'Neonode.model.GeneratorModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'GeneratorModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    menu: function ( page ) {

        var response = [
            '<div id="menuWrapper1" class="menu-wrapper1">',
            '<div class="menu-auxbg1"></div>',
            '<div id="menuWrapper2" class="menu-wrapper2 menu-font">',
            '<div id="menuItemWrapperHome" class="menu-1st-row-outer">',
            '<a href="#homePage" id="menuItemPanelHome" class="menu-item menu-1st-row-inner' + (page === "HOME" ? ' active-page' : '') + '">',
            '<span class="menu-item menu-item-filler">HOME</span>',
            '<span id="menuItemTextHome" class="menu-item menu-1st-row-text">HOME</span>',
            '</a>',
            '</div>',
            '<div id="menuItemWrapperFutureConcept" class="menu-1st-row-outer">',
            '<center style="height: 100%;">',
            '<a href="#futureConceptPage" id="menuItemPanelFutureConcept" class="menu-item menu-1st-row-inner' + (page === "FUTURE CONCEPT" ? ' active-page' : '') + '">',
            '<span class="menu-item menu-item-filler">FUTURE CONCEPT</span>',
            '<span id="menuItemTextFutureConcept" class="menu-item menu-1st-row-text">FUTURE CONCEPT</span>',
            '</a>',
            '</center>',
            '</div>',
            '<div id="menuItemWrapperAbout" class="menu-1st-row-outer">',
            '<a href="#aboutPage" id="menuItemPanelAbout" class="menu-item menu-1st-row-inner' + (page === "ABOUT" ? ' active-page' : '') + '">',
            '<span class="menu-item menu-item-filler">ABOUT</span>',
            '<span id="menuItemTextAbout" class="menu-item menu-1st-row-text">ABOUT</span>',
            '</a>',
            '</div>',
            '<div id="menuItemWrapperProducts" class="menu-2nd-row-outer">',
            '<a href="#productsPage" id="menuItemPanelProducts" class="menu-item menu-2nd-row-inner' + (page === "PRODUCTS" ? ' active-page' : '') + '">',
            '<span class="menu-item menu-item-filler">PRODUCTS</span>',
            '<span id="menuItemTextProducts" class="menu-item menu-2nd-row-text">PRODUCTS</span>',
            '</a>',
            '</div>',
            '<div id="menuItemWrapperNewsroom" class="menu-2nd-row-outer">',
            '<center style="height: 100%;">',
            '<a href="#newsroomPage" id="menuItemPanelNewsroom" class="menu-item menu-2nd-row-inner' + (page === "NEWSROOM" ? ' active-page' : '') + '">',
            '<span class="menu-item menu-item-filler">NEWSROOM</span>',
            '<span id="menuItemTextNewsroom" class="menu-item menu-2nd-row-text">NEWSROOM</span>',
            '</a>',
            '</center>',
            '</div>',
            '<div id="menuItemWrapperContact" class="menu-2nd-row-outer">',
            '<a href="#contactPage" id="menuItemPanelContact" class="menu-item menu-2nd-row-inner' + (page === "CONTACT" ? ' active-page' : '') + '">',
            '<span class="menu-item menu-item-filler">CONTACT</span>',
            '<span id="menuItemTextAnchorContact" class="menu-item menu-2nd-row-text">CONTACT</span>',
            '</a>',
            '</div>',
            '</div>',
            '</div>'
        ];

        return response.join( '' );
    },
    submenu: function ( config ) {

        var response = [];
        for ( var i = 0, j = config.length;
              i < j;
              i++ ) {
            response.push( '<div class="submenu-label menu-font">' + config[i].label + ':</div><div class="submenu-items menu-font">' );
            for ( var tmp, items = config[i].items, k = 0, l = items.length;
                  k < l;
                  k++ ) {
                tmp = '<a href="#' + items[k].page + '" class="' + (items[k].page === config[i].activeItem ? ' active-item' : 'submenu-item') + '">' + items[k].label + '</a>';
                if ( (k + 1) < l ) {
                    if ( items[k].bpMobile ) {
                        response.push( tmp + '<span class="submenu-bp-m"> | </span>' );
                    }
                    else {
                        if ( items[k].bpTablet ) {
                            response.push( tmp + '<span class="submenu-bp-t"> | </span>' );
                        }
                        else {
                            response.push( tmp + '<span> | </span>' );
                        }
                    }
                } else {
                    response.push( tmp + '</div>' );
                }
            }
        }

        return response.join( '' );
    },
    newsPuffs: function ( id, items ) {
        var response = [];
        console.error('items:');
        console.error(items);
        for ( var text, tmp, i = 0, j = items.length;
              i < j;
              i++ ) {
            if ( !(i % 2) ) {
                response.push( '<div class="news-puff-wrapper">' );
            }
            response.push( '<div class="news-puff"><div class="news-puff-bg"><div class="news-puff-pc"><div class="news-puff-title">' );
            response.push( '<h3>' + items[i].data.title + '</h3><p class="pubnotice">' + items[i].data.date + '</p>' );
            response.push( '</div><div class="news-puff-body">' );
            response.push( items[i].data.body[0] + '</div></div><div class="news-puff-badge news-puff-badge-' + items[i].data.category.toLowerCase() + '"></div>' )
            response.push( '<div class="news-puff-more"><p><a href="#' + id + '?node=' + items[i].id.split('-')[2] + '" class="submenu-item">Read more</a></p></div></div></div>' );
            if ( i % 2 ) {
                response.push( '</div>' );
            }
        }
        if ( items.length % 2 ) {
            response.push( '</div>' );
        }
        return response.join( '' );
    },
    newsDetails: function ( storeIndex, id, backLocation ) {
        console.error('storeIndex: ');
        console.error(storeIndex);
        console.error('id: ');
        console.error(id);
        console.error('backLocation: ');
        console.error(backLocation);
        var record = Ext.getStore( 'NewsroomStore' ).getAt( storeIndex ),
            contentType = record.data.category.toLowerCase() === "news" ? ["News", "News"] : [
                "Press release", "Press releases"
            ],
            response = [
                '<div class="content-wrapper1">',
                this.submenu( [
                    {
                        label: 'Navigate',
                        activeItem: id,
                        items: [
                            {
                                label: 'Return to overview',
                                page: backLocation
                            },
                            {
                                label: contentType[0] + ' details',
                                page: id
                            }
                        ]
                    }
                ] ),
                '<div class="content-wrapper2">',
                '<h1>' + record.data.title + '</h1>',
                '<p class="pubnotice">Published ' + record.data.date + ' in ' + contentType[1] + '</p>',
                record.data.body[1],
                '</div><div class="content-wrapper3"></div>',
                '</div>'
            ];

        return response.join( '' );
    },
    footer: function () {
        var width = 320,
            height = width / (480 / 102),
            response = [
                '<div id="footerWrapper1" class="footer-wrapper1" style="height: ' + height + 'px;">',
                '<div class="footer-wrapper2">',
                '<a href="#homePage" id="footerItemAnchorHome" class="menu-item footer-anchor"></a>',
                '<a href="#contactPage" id="footerItemAnchorContact" class="menu-item footer-anchor"></a>',
                '<a href="#loginPage" id="footerItemAnchorLogin" class="menu-item footer-anchor"></a>',
                '<a href="#investorRelationsPage" id="footerItemAnchorInvestor" class="menu-item footer-anchor"></a>',
                '</div>',
                '</div>'
            ];

        return response.join( '' );
    }
} );
