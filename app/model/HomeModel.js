Ext.define( 'Neonode.model.HomeModel', {
    extend: 'Ext.data.Model',
    alternateClassName: 'HomeModel',
    singleton: true,

    config: {
        fields: [
            { name: 'Model', type: 'auto' }
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.store.NewsroomStore'
    ],

    store: 'NewsroomStore',

    intro: function ( id ) {
        console.error('HomeModel.intro(id): ');
        console.error(id);
        var store = Ext.getStore( 'NewsroomStore' ),
            tmp = [
                '<div class="content-wrapper1">',
                '<div class="content-wrapper2 t-layout">',
                '<h1>Recent news and press releases</h1>',
                (store ? '</div>' + GeneratorModel.newsPuffs( id.split( '-' )[0] + '-view', store.getRange( 0, 3 ) ) : '<p class="loading">[ Loading news and press releases . . . ]</p></div>'),
                '<div class="content-wrapper2 t-layout"><h1>Neonode is hiring</h1>',
                '<p>We are now hiring technical talents. In 2012 we will double our efforts, increase our team and expand our market, and we want to engage new competences to be a part of that success!<br>',
                '<br>Check out our open positions over at <a href="#aboutPage-career" class="submenu-item">About &gt; Career</a></p>',
                '</div><div class="content-wrapper3"></div>',
                '</div>'
            ];

        return tmp.join( '' );
    }
} );
