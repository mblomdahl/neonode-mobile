Ext.define( 'Neonode.controller.ContentFeedr', {
    extend: 'Ext.app.Controller',

    id: 'ContentFeedr',

    config: {
        profile: Ext.os.deviceType.toLowerCase(),
        activePage: 'homePage-intro',
        entryHash: window.location.hash,
        stores: [
            'NewsroomStore'
        ],
        refs: {
            'AboutPage': {
                selector: 'aboutpage',
                autoCreate: true,
                xtype: 'aboutpage'
            },
            'ContactPage': {
                selector: '#contactPage',
                autoCreate: true,
                xtype: 'contactpage'
            },
            'FutureConceptPage': {
                selector: '#futureConceptPage',
                autoCreate: true,
                xtype: 'futureconceptpage'
            },
            'HomePage': {
                selector: '#homePage',
                autoCreate: true,
                xtype: 'homepage'
            },
            'InvestorRelationsPage': {
                selector: '#investorRelationsPage',
                autoCreate: true,
                xtype: 'investorrelationspage'
            },
            'NewsroomPage': {
                selector: '#newsroomPage',
                autoCreate: true,
                xtype: 'newsroompage'
            },
            'ProductsPage': {
                selector: '#productsPage',
                autoCreate: true,
                xtype: 'productspage'
            }
        },
        control: {

        },
        views: [
            'AboutPage',
            'ContactPage',
            'FutureConceptPage',
            'HomePage',
            'InvestorRelationsPage',
            'NewsroomPage',
            'ProductsPage'
        ]
    },

    requires: [
        'Neonode.model.GeneratorModel',
        'Neonode.model.NewsroomModel',
        'Neonode.model.HomeModel',
        'Neonode.model.ContactModel',
        'Neonode.model.FutureConceptModel',
        'Neonode.model.InvestorRelationsModel',
        'Neonode.model.ProductsModel',
        'Neonode.model.AboutModel',
        'Neonode.store.NewsroomStore'
    ],

    init: function () {
        this.control( {
            'aboutpage': {
                show: this.onPanelRendered
            }
        } );


        var height = Ext.getBody().getWidth() / (480 / 102);
        height = height > 102 ? 102 : Math.round( height );
        height = 'height: ' + height + 'px;';
        Ext.Array.each( Ext.query( '#footerWrapper1.footer-wrapper1' ), function ( footer ) {
            footer.style.cssText = height;
        } );
        myGlobals.contentFeedr = this;
        //myGlobals.argz = argz;
        var hash = this.getEntryHash(),
            caller = this,
            store = Ext.getStore( 'NewsroomStore' );
        if ( hash.length && hash.split( '?' ).length === 1 ) {
            this.navigate( hash );
            hash = "";
        }

        if ( store ) {
            store.addListener( 'load', function () {
                var newsListing = caller.getHomePage();
                newsListing.innerItems[0].items['items'][1].setHtml( HomeModel.intro( 'homePage-intro' ) );

                newsListing = caller.getNewsroomPage().getItems().map;
                newsListing['newsroomPage-intro'].items['items'][1].setHtml( NewsroomModel.intro( 'newsroomPage-intro' ) );
                newsListing['newsroomPage-press'].items['items'][1].setHtml( NewsroomModel.press( 'newsroomPage-press' ) );
                newsListing['newsroomPage-news'].items['items'][1].setHtml( NewsroomModel.news( 'newsroomPage-news' ) );

                if ( hash.length ) {
                    caller.navigate( hash );
                }
            } );
            store.load();
        }

        var contentFeedr = this;
        Ext.Viewport.element.on( {
            tap: function ( event, node ) {
                event.stopEvent();
                var classes = [];
                if ( !event.target.classList ) {
                    event.target.classList = event.target.getAttribute( 'class' ).split( ' ' );
                }
                for ( var tmp = event.target.classList, i = tmp.length;
                      i--; ) {
                    classes.push( tmp[i] );
                }
                if ( event.target.tagName.toLowerCase() !== "a" ) {
                    event.target = event.target.parentElement;
                    if ( event.target.tagName.toLowerCase() !== "a" ) {
                        return false;
                    }
                }
                if ( classes.indexOf( 'menu-item' ) !== -1 || classes.indexOf( 'submenu-item' ) !== -1 ) {
                    console.error( 'navigate triggered: ' );
                    console.error( event.target.href );
                    contentFeedr.navigate( event.target.href.match( /#.+$/ )[0] );
                } else if ( classes.indexOf( 'inline-anchor' ) !== -1 ) {
                    window.location = event.target.href;
                }
                return false;
            }
        } );
    },

    //called when the Application is launched, remove if not needed
    launch: function ( app ) {

        //window.setTimeout(function() {

        this.getHomePage();
        this.getAboutPage();
        this.getContactPage();
        this.getInvestorRelationsPage();
        this.getFutureConceptPage();
        this.getProductsPage();
        this.getNewsroomPage();


        //}, 2000);
    },

    navigate: function ( target ) {
        target = target.substring( 1 );
        console.log( "target:" + target );
        var targetComp = target.split( '?' ),
            pageMapping = Ext.Viewport.getItems().map,
            targetPage = targetComp[0].split( '-' ),
            targetParams = targetComp[1],
            l1Page = undefined,
            l1Changed = false,
            l2Page = undefined,
            l2Changed = false,
            l3Page = undefined,
            l3Changed = false,
            hasIntro = false,
            animDuration = null;

        if ( !(l1Page = pageMapping[targetPage[0]]) ) {
            return false;
        }

        if ( !(targetPage[1] || (hasIntro = l1Page.getItems().map[targetPage[0] + '-intro'])) ) { // go to l1Page
        } else {
            if ( !hasIntro && !(l2Page = l1Page.getItems().map[targetPage[0] + '-' + targetPage[1]]) ) {
                return false;
            }
            if ( hasIntro ) { // go to l2Page
                l2Page = hasIntro;
            } else {
                if ( !targetPage[2] ) { // go to l2Page
                } else {
                    if ( !(l3Page = l2Page.getItems().map[targetPage[0] + '-' + targetPage[1] + '-' + targetPage[2]]) ) {
                        return false;
                    }
                    // else go to l3Page
                }
            }
        }

        function animate() {
            Ext.Anim.run( Ext.Viewport, 'fade', {
                out: false,
                duration: animDuration,
                easing: "ease-in-out",
                autoClear: false
            } );
        }

        function asXType( id ) {
            return '#' + id; //.toLowerCase();
        }

        // check transit need
        if ( l1Changed = Ext.Viewport.getActiveItem().id !== l1Page.id ) {
            animDuration = 500;
        }
        if ( l2Page && (l2Changed = l1Page.getActiveItem().id !== l2Page.id) ) {
            if ( !animDuration ) {
                animDuration = 350;
            }
        }
        if ( l3Page && (l3Changed = l3Page.getActiveItem().id !== l3Page.id) ) {
            if ( !animDuration ) {
                animDuration = 200;
            }
        }

        if ( !targetParams && (l1Changed || l2Changed || l3Changed) ) { // submit std transit

            animate();
            console.log('animate!');
            if ( l3Changed ) {
                console.log('l3changed');
                l2Page.setActiveItem( asXType( l3Page.id ) );
            }

            if ( l2Changed ) {
                console.log('l2changed');
                l1Page.setActiveItem( asXType( l2Page.id ) );
            }

            if ( l1Changed ) {
                console.log('l1changed');
                Ext.Viewport.setActiveItem( asXType( l1Page.id ) );
            }

        } else if ( targetParams ) {

            var paramComp = targetParams.split( '=' );
            targetParams = {
                id: target,
                backLocation: this.getActivePage()
            };
            targetParams[paramComp[0]] = paramComp[1];

            animate();

            if ( !l1Page.populate( targetParams ) ) {
                return false;
            }

            if ( l3Changed ) {
                console.log('l3changed2');
                l2Page.setActiveItem( asXType( l3Page.id ) );
            }

            if ( l2Changed ) {
                console.log('l2changed2');
                l1Page.setActiveItem( asXType( l2Page.id ) );
            }

            if ( l1Changed ) {
                console.log('l1changed2');
                Ext.Viewport.setActiveItem( asXType( l1Page.id ) );
            }

        } else {
            return false;
        }

        window.location.hash = '#' + target;
        this.setActivePage( target );

    },

    onPanelRendered: function () {
        return true;
    }

} );
