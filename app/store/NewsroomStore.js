Ext.define( 'Neonode.store.NewsroomStore', {
    requires: [
        'Ext.DateExtras'
    ],
    extend: 'Ext.data.Store',
    config: {
        storeId: "NewsroomStore",
        autoLoad: false,
        proxy: {
            url: 'resources/json/NewsroomStore.js',
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'newsroomContent'
            }
        },
        sorters: [
            { property: 'date', direction: 'DESC' }
        ],
        filters: [
            { property: 'category', value: /(news|press)/i }
        ],
        fields: [
            {
                name: 'category',
                type: 'string'
            },
            {
                name: 'date',
                type: 'date',
                dateFormat: 'Y-m-d',
                convert: function ( v, d ) {
                    return Ext.Date.format( Ext.Date.parse( v, 'Y-m-d' ), 'F j, Y' );
                }
            },
            {
                name: 'title',
                type: 'string'
            },
            {
                name: 'body',
                convert: function ( v, d ) {
                    if ( !v || !v.length ) {
                        return undefined;
                    }

                    var body = v,
                        adjustment = Math.round( d.data.title.length * 1.5 ),
                        lowerLim = 340 - adjustment,
                        upperLim = 440 - adjustment,
                        tmp = body[0].length,
                        text = "",
                        tagged = false,
                        response = [];

                    if ( body.length === 1 || (tmp > lowerLim && tmp < upperLim) ) {
                        text = body[0];
                        tagged = true;
                    } else {
                        tmp += body[1].length;
                        if ( tmp > lowerLim && tmp < upperLim ) {
                            text = body[0] + body[1];
                            tagged = true;
                        } else {
                            for ( var avgLen = lowerLim + Math.round( (upperLim - lowerLim) / 2 ), i = 0, j = body.length;
                                  i < j;
                                  i++ ) {
                                if ( tmp = body[i].match( />[^<]+</g ) ) {
                                    for ( var auxTmp1, auxTmp2, auxTmp3, auxTmp4, k = 0, l = tmp.length;
                                          k < l;
                                          k++ ) {
                                        tmp[k] = tmp[k].split( /[<>]/ )[1];
                                        if ( (tmp[k].length + text.length) > lowerLim ) {
                                            auxTmp1 = tmp[k].length > (avgLen - text.length) ? (avgLen - text.length) : tmp[k].length;
                                            auxTmp3 = tmp[k].substring( 0, auxTmp1 );
                                            auxTmp2 = auxTmp3.lastIndexOf( '.' );
                                            auxTmp4 = auxTmp3.lastIndexOf( ',' );
                                            auxTmp2 = Math.max( auxTmp4, auxTmp2 );
                                            if ( auxTmp2 !== -1 && (auxTmp2 + text.length) > lowerLim ) {
                                                text += tmp[k].substring( 0, auxTmp2 ) + ' ...';
                                            }
                                            else {
                                                text += tmp[k].substring( 0, auxTmp3.lastIndexOf( ' ' ) ) + '...';
                                            }
                                            i = j;
                                            break;
                                        } else {
                                            text += tmp[k] + ' ';
                                        }
                                    }
                                }
                            }
                            text = '<p>' + text + '</p>';
                        }
                    }
                    if ( !text.length ) {
                        text = '[ Parsing error ]';
                    }
                    return [text, body.join( ' ' )];
                }
            },
            {
                name: 'image'
            },
            {
                name: 'video'
            }
        ]
        // itemTpl: '{date}: {title}'
    }
});
